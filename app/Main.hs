{-# LANGUAGE OverloadedStrings #-}
module Main where

import Lib
import Control.Arrow (Kleisli(..), second)

import qualified Data.Text as T
import Data.Text.IO as TIO

import Text.Printf (printf)

type RawPoint = (T.Text, Row)
type ProcessedPoint = (T.Text, Double, Maybe Implication)

-- True for positive implication, False for negative
type Implication = Bool

main :: IO ()
main = do
  rawPoints <- parseRawPoints <$> TIO.readFile inputPath
  let points = map processPoint rawPoints
  let goodPoints = filter isGood points
  let implPoints = filter hasImplication goodPoints

  let result1Text = showGoodPoints $ goodPoints
  TIO.writeFile result1Path result1Text

  let result2Text = showImplPoints $ implPoints
  TIO.writeFile result2Path result2Text
  

    where parseRawPoints = map parseRawPoint . T.lines
          showGoodPoints = T.unlines . map showGoodPoint
          showImplPoints = T.unlines . map showImplPoint

          processPoint (name, row) =
              (name, pValue row, getImplication row)

          isGood (_, p, _) = p < 0.05
                             
          hasImplication (_, _, Just _) = True
          hasImplication _ = False

          parseRawPoint :: T.Text -> RawPoint
          parseRawPoint s =
              case T.splitOn " " s of
                (name:rest) -> (name, map (read . T.unpack) rest)

          showGoodPoint :: ProcessedPoint -> T.Text
          showGoodPoint (name, p, _) =
              name `T.append` (T.pack $ printf " %.5f" p)

          showImplPoint :: ProcessedPoint -> T.Text
          showImplPoint (name, _, Just impl) =
              name `T.append` (T.pack $ printf " %s" (showImpl impl))

          showImpl :: Implication -> String
          showImpl impl = if impl then "T" else "F"

          inputPath = "Input.txt"
          result1Path = "Result_1.txt"
          result2Path = "Result_2.txt"              

kleisliSecond :: (Monad m) => (b -> m c) -> (a, b) -> m (a, c)
kleisliSecond = runKleisli . second . Kleisli

-- | Just True for positive implication
--   Just False for negative implication
--   None in all other cases
getImplication :: Row -> Maybe Implication
getImplication row
    | t `elem` positiveImplicationTables = Just True
    | t `elem` negativeImplicationTables = Just False
    | otherwise = Nothing
    where t = findTruthTable row

-- x => y
positiveImplicationTables :: [TruthTable]
positiveImplicationTables = [1, 5, 9, 13]

-- x => not y
negativeImplicationTables :: [TruthTable]
negativeImplicationTables = [2, 6, 10, 14]
