module Lib where

import Data.List (foldl1')
import Data.Function (on)
import Control.Arrow ((&&&))
import Data.Bits ((.&.))
    
import Math.Combinatorics.Exact.Binomial (choose)
import Math.Combinat.Sets (combine)
    

-- | List of 4
type Row = [Integer]

-- | List of 4
type NormalizedRow = [Double]

-- | Truth table of a boolean function from two arguments
type TruthTable = Int

unpackTable :: TruthTable -> [Int]
unpackTable t = map (\n -> (t .&. n) `div` n) [8, 4, 2, 1]

normalize :: Row -> NormalizedRow
normalize = impl . map fromIntegral
    where impl xs = map (/ maximum xs) xs

pValue :: Row -> Double
pValue [a, b, c, d] = fromRational $ ((/) `on` fromIntegral) num denom
    where num = choose (a + b) a * choose (c + d) c
          denom = choose (a + b + c + d) (a + c)

-- | Quality of corellation between the row and given truth table (1 - \sigma)
truthTableQuality :: TruthTable -> NormalizedRow -> Double
truthTableQuality tPacked r = 1 - sigma
    where sigma =  sqrt . (/4) . sum . map sqr $
                   zipWith (-) r t
          t = map fromIntegral $ unpackTable tPacked
          sqr a = a * a                        

truthTables :: [TruthTable]
truthTables = [0..15]

-- | Find the truth table that best describes given row
findTruthTable :: Row -> TruthTable
findTruthTable row = snd .
                     maximum .
                     map (quality &&& id) $
                     truthTables
    where quality t = truthTableQuality t normRow
          normRow = normalize row
